Sonoff controller
=================

A Sonoff is [this thing](http://wiki.iteadstudio.com/Sonoff), it's basically a
a wifi-enabled plug, with the appealing property that it runs on an ESP8266
microcontroller, making it much less hackable (in a "honey, there's
a suspicious van parked across the street" sense, not in a "I just electrocuted
myself" sense), which is great.

I didn't like the default firmware because it routes all requests through
a server in China (for all I know), or, even worse, the US. Since there's no way
to disable that behaviour that I found after not searching at all, I decided to
write the ten lines it took to make my own firmware.

Luckily, [arendst has done most of the
work](https://github.com/arendst/Sonoff-MQTT-OTA), and his seems like an
excellent project, but I open-source most of my things, so here it is. Do
whatever you want with this code (MIT license). I don't imagine it will be very
useful to you, but feel free to open pull requests or cans of beer or anything
else you want.

-- Stavros
